import React from "react";
import { StyleSheet, View, Image, Text, TouchableOpacity } from "react-native";
import Box from "../assets/box.png";
import Aix from "../assets/Aix.png";
import Zero from "../assets/Zero.png";
import OIcon from "../assets/RedZero.png";
import XIcon from "../assets/Xicon.png";

import winnerIcon from "../assets/winnerIcon.png";
import ResetIcon from "../assets/resetIcon.png";

let Tic_Tac_Toe = function (props) {
  const { board, onClick, gameBreak, getindex, reset } = props;
  return (
    <>
      {gameBreak.length == 3 && (
        <View style={styles.winnerBox}>
          <Image style={styles.winnerBoxicon} source={winnerIcon} />
          <View style={styles.winnerTextBox}>
            <Text style={styles.winnerText}>Winner</Text>
            <Text style={styles.winner}>
              {getindex == "o" ? "Aix" : "Zero"}
            </Text>
          </View>
          <TouchableOpacity onPress={reset} style={styles.winnerResetSection}>
            <Text style={styles.resetText}>Reset</Text>
            <View>
              <Image style={styles.winnerBoxicon} source={ResetIcon} />
            </View>
          </TouchableOpacity>
        </View>
      )}
      {/* <View style={styles.center}> */}
      <View style={styles.rowBox}>
        {board.map((row, i) => {
          return (
            <TouchableOpacity onPress={() => onClick(i)}>
              <View key={i} style={styles.rowContent}>
                {row.value && row.value == "X" ? (
                  <Image style={styles.icon} source={XIcon} />
                ) : null}
                {row.value && row.value == "O" ? (
                  <Image style={styles.icon} source={OIcon} />
                ) : null}
                {row.value && row.value == "null" && (
                  <Image style={styles.icon} source={Box} />
                )}
                {row.value && row.value == "x" ? (
                  <Image style={styles.icon} source={Aix} />
                ) : null}
                {row.value && row.value == "o" ? (
                  <Image style={styles.icon} source={Zero} />
                ) : null}
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
      {/* </View> */}
    </>
  );
};

const styles = StyleSheet.create({
  rowBox: {
    width: 300,
    height: 300,
    backgroundColor: "white",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    flexWrap: "wrap",
    borderRadius: 8,
    padding: 2,
  },
  rowContent: {
    width: 90,
    height: 90,
    margin: 4,
    borderRadius: 8,
  },
  icon: {
    width: "100%",
    height: "100%",
  },
  winnerTextBox: {
    marginLeft: 10,
    marginRight: 10,
  },
  winnerBox: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 10,
    backgroundColor: "#ffff",
  },
  winnerBoxicon: {
    marginRight: 10,
    width: 40,
    height: 40,
  },
  winnerText: {
    color: "#6a65e7",
    fontSize: 35,
  },
  winner: {
    color: "#888",
    fontSize: 30,
  },
  winnerResetSection: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  resetText: {
    fontSize: 20,
  },
});

export default Tic_Tac_Toe;

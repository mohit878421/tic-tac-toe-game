import React from "react";
import { StyleSheet, View, Image } from "react-native";
import Board from "./src/Board";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Logo from "./assets/Logo.png";

let _board = [
  { value: "null" },
  { value: "null" },
  { value: "null" },
  { value: "null" },
  { value: "null" },
  { value: "null" },
  { value: "null" },
  { value: "null" },
  { value: "null" },
];
export default function App() {
  let [getindex, setGetindex] = React.useState("o");
  let [gameBreak, setGameBreak] = React.useState([]);
  let [board, setBoard] = React.useState(_board);
  let [hack, setHack] = React.useState(false);
  let reset = async () => {
    const myArray = await AsyncStorage.getItem("board");
    if (gameBreak.length == 3) {
      setBoard(JSON.parse(myArray));
      setGameBreak([]);
      setGetindex("o");
    }
  };
  let onClick = function (i) {
    if (gameBreak.length == 3) {
      return;
    }
    if (hack == false) {
      setHack(true);
      AsyncStorage.setItem("board", JSON.stringify(board));
    }
    if (board[i].value !== "x" && board[i].value !== "o") {
      board[i].value = getindex;
      setBoard(board);
    }
    let checkArray = [
      [
        { cValue: board[0].value, index: "0" },
        { cValue: board[1].value, index: "1" },
        { cValue: board[2].value, index: "2" },
      ],
      [
        { cValue: board[3].value, index: "3" },
        { cValue: board[4].value, index: "4" },
        { cValue: board[5].value, index: "5" },
      ],
      [
        { cValue: board[6].value, index: "6" },
        { cValue: board[7].value, index: "7" },
        { cValue: board[8].value, index: "8" },
      ],
      [
        { cValue: board[0].value, index: "0" },
        { cValue: board[4].value, index: "4" },
        { cValue: board[8].value, index: "8" },
      ],
      [
        { cValue: board[2].value, index: "2" },
        { cValue: board[4].value, index: "4" },
        { cValue: board[6].value, index: "6" },
      ],
      [
        { cValue: board[0].value, index: "0" },
        { cValue: board[3].value, index: "3" },
        { cValue: board[6].value, index: "6" },
      ],
      [
        { cValue: board[1].value, index: "1" },
        { cValue: board[4].value, index: "4" },
        { cValue: board[7].value, index: "7" },
      ],
      [
        { cValue: board[2].value, index: "2" },
        { cValue: board[5].value, index: "5" },
        { cValue: board[8].value, index: "8" },
      ],
    ];
    for (let i = 0; i <= checkArray.length - 1; i++) {
      let tempArray = checkArray[i];
      let gamearray = [];
      let temp = 0;
      for (let j = 0; j <= tempArray.length - 1; j++) {
        if (tempArray[j].cValue == getindex) {
          gamearray.push(tempArray[j].index);
        } else {
          break;
        }
        temp += 1;
      }
      if (gamearray.length == 3) {
        board[gamearray[0]].value = getindex == "x" ? "X" : "O";
        board[gamearray[1]].value = getindex == "x" ? "X" : "O";
        board[gamearray[2]].value = getindex == "x" ? "X" : "O";
        setBoard(board);
        setGameBreak(gamearray);
      }
      if (temp == 3 && gamearray.length != 3) {
        checkArray.splice(i, 1);
      }
    }
    setGetindex(getindex == "x" ? "o" : "x");
  };
  return (
    <View style={styles.container}>
      <Image style={styles.icon} source={Logo} />

      <Board
        board={board}
        onClick={onClick}
        gameBreak={gameBreak}
        getindex={getindex}
        reset={reset}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: "center",
    justifyContent: "center",
  },
  icon: {
    width: "100%",
    height: 100,
    padding: 10,
    marginBottom: 100,
  },
});
